<?php

namespace Drupal\config_pages_viewer\Controller;

use Drupal\config_pages\Entity\ConfigPages;
use Drupal\config_pages\Entity\ConfigPagesType;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class HomePageController.
 */
class ConfigPageViewerController extends ControllerBase {

  /**
   * Sho config page.
   *
   * @param Drupal\config_pages\Entity\ConfigPagesType $config_page_type
   *   Config page name.
   *
   * @return array
   *   Render Array.
   */
  public function show(ConfigPagesType $config_page_type = NULL) {

    if ($config_page_type != NULL) {
      $config_page = ConfigPages::load($config_page_type->id());

      if ($config_page != NULL) {
        // Correct metatags attachment.
        if (function_exists('metatag_get_tags_from_route')) {
          $metatag_attachments = &drupal_static('metatag_attachments');
          $metatag_attachments = metatag_get_tags_from_route($config_page);
        }

        return parent::entityTypeManager()->getViewBuilder('config_pages')->view($config_page, 'full');

      }
    }

    return ['#markup' => 'no page'];
  }

}
